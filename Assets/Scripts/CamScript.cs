﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour
{
    public static CamScript instance;

    void Awake ()
    {
        instance = this;
    }

    public void Configure(Transform parent)
    {
        gameObject.transform.parent = parent;
        gameObject.transform.localPosition = new Vector3(0, 100, 0);
        gameObject.transform.localRotation = Quaternion.Euler(90f, 0, 0);
    }
}
