﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviourPunCallbacks
{
    private PhotonView PV;
    private CharacterController myCC;
    public float movementSpeed;
    public float rotationSpeed;
    public GameObject cameraParent;

    void Start ()
    {
        PV = GetComponent<PhotonView>();
        myCC = GetComponent<CharacterController>();
        cameraParent.SetActive(photonView.IsMine);
    }

    private void Update()
    {
        if (!photonView.IsMine) return;
    }

    private void FixedUpdate()
    {
        if (!photonView.IsMine) return;
    }
} 
