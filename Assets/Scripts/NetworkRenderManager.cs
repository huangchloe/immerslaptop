﻿using UnityEngine;
using Photon.Pun;

public class NetworkRenderManager : MonoBehaviour
{
    private PhotonView photonView;

        private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void setModelActive(bool status)
    {
            Debug.Log("Gameobject is " + this.transform.gameObject);
           // this.transform.gameObject.SetActive(true);
            photonView.RPC("RpcClientEnable", RpcTarget.AllBuffered, status);
       
    }

    [PunRPC]
    void RpcClientEnable(bool status)
    {
        this.transform.gameObject.SetActive(status);
    }

   
}
