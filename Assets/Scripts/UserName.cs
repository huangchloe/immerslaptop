﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UserName : MonoBehaviourPun
{
    [SerializeField] private TextMesh nameText;

    private void Start()
    {
        if (photonView.IsMine) { return; }

    }

    private void SetName()
    {
        nameText.text = photonView.Owner.NickName;
    }
}
