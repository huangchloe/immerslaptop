﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RPCPhotonView : MonoBehaviourPunCallbacks
{
    private PhotonView PV;
    public GameObject testCube;
    public bool cubeBool = true;

    public byte tagCube;

    void Update()
    {
        UpdateCube();
    }

    public void UpdateCube()
    {
        if (PV.IsMine)
        {
            PV.RPC("UpdateCubeMsg", RpcTarget.AllBuffered, tagCube);

        }
    }

    [PunRPC]
    void UpdateCubeMsg(byte _tagCube)
    {
        switch (_tagCube)
        {
            case 0:
                testCube.SetActive(true);
                break;

            case 1:
                testCube.SetActive(false);
                break;
        }
    }

    /*public void CallRemoteMethod()
  {
      PV = GetComponent<PhotonView>();
      cubeBool = testCube.activeSelf;

      PV.RPC("SyncCube", RpcTarget.AllBuffered, cubeBool);
  }

  [PunRPC]
  public void showCube (bool cubeBool)
  {
      PV.FindObservables(cubeBool = true);
  }

  [PunRPC]
  public void hideCube (bool cubeBool)
  {
      PV.FindObservables(cubeBool = false);
  }

  void SyncCube (bool cubeBool)
  {
     if (cubeBool == true)
      {
          if (PV.IsMine)
          {
              PV.RPC("MyRemoteMethod", RpcTarget.AllBuffered, testCube);
          }
      } else
      {
          if (PV.IsMine)
          {
              PV.RPC("MyRemoteMethod", RpcTarget.AllBuffered, testCube);
          }
      }
  }*/
}
