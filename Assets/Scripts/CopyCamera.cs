﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyCamera : MonoBehaviourPunCallbacks
{

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
            this.transform.position = Camera.main.transform.position;
    }
}
